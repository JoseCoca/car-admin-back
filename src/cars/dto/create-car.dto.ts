import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsDateString, IsNotEmpty, IsNumber, IsOptional, IsString, IsUrl } from "class-validator";

export class CreateCarDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  description: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  make: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  model: string;

  @ApiProperty()
  @IsDateString()
  @IsOptional()
  estimateDate: string;

  @ApiProperty()
  @IsNumber()
  @IsOptional()
  km: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsUrl()
  image: string;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  maintenance: boolean;
}
