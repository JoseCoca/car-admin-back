import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCarDto } from './dto/create-car.dto';
import { UpdateCarDto } from './dto/update-car.dto';
import { Car } from './entities/car.entity';

@Injectable()
export class CarsService {
  constructor(
    @InjectRepository(Car)
    private carsRepository: Repository<Car>,
  ) {}

  create(createCarDto: CreateCarDto) {
    return this.carsRepository.save(createCarDto);
  }

  async findAll() {
    return await this.carsRepository.find();
  }

  async findOne(id: number) {
    return await this.carsRepository.findOne(id);
  }

  update(id: number, updateCarDto: UpdateCarDto) {
    return this.carsRepository.save(updateCarDto);
  }

  remove(id: number) {
    return this.carsRepository.delete(id);
  }
}
