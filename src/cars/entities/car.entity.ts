import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Car extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'text' })
  description: string;

  @Column()
  make: string;

  @Column()
  model: string;

  @Column({ type: 'date', nullable: true })
  estimateDate: Date;

  @Column({ nullable: true })
  km: number;

  @Column()
  image: string;

  @Column({ nullable: true, default: false })
  maintenance: boolean;
}
